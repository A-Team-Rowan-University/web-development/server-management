from asgiref.sync import sync_to_async, async_to_sync
from django.apps import AppConfig
from server_management import servers
import asyncio


async def start_proxy():
    asyncio.create_task(sync_to_async(servers.start_proxy)())


class ServerManagementAppConfig(AppConfig):
    name = 'server_management'
    verbose_name = 'Server Management'

    def ready(self):
        async_to_sync(start_proxy)()
