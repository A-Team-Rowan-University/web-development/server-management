import json

import asyncio

from asgiref.sync import sync_to_async

from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.conf import settings

from server_management.servers import start_server, remove_server, list_servers


def view_index(request):
    print(request.META)

    host = request.get_host()
    port = settings.MANAGE_PORT

    host = '.'.join(host.split('.')[1:]) + ":" + str(port)

    servers = list_servers();

    context = {
        'servers': servers,
        'host': host,
    }

    return render(request, 'index.html', context)


async def view_start_server(request):
    if request.method == "POST":
        server_id = request.POST['id']
        asyncio.create_task(sync_to_async(start_server)(server_id))

    return redirect(view_index)


async def view_reload_server(request, server_id):
    asyncio.create_task(sync_to_async(start_server)(server_id))

    return redirect(view_index)


async def view_remove_server(request, server_id):
    asyncio.create_task(sync_to_async(remove_server)(server_id))

    return redirect(view_index)


async def view_gitlab(request):
    if request.method == 'POST':
        body = json.loads(request.body)
        status = body['object_attributes']['status']
        ref = body['object_attributes']['ref']
        if status == 'success':
            print(ref)
            asyncio.create_task(sync_to_async(start_server)(ref))

    return HttpResponse(b'OK', status=200)

view_gitlab.csrf_exempt = True