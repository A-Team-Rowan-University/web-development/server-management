import os
import docker
from django.conf import settings


class ServerContainer:
    def __init__(self, name, image, host_ports, container_ports):
        self.name = name
        self.image = image.split('/')[-1] if image is not None else ''
        self.host_ports = host_ports
        self.container_ports = container_ports


class Server:
    def __init__(self, sid: str, web: ServerContainer, db: ServerContainer, nginx: ServerContainer):
        self.id = sid
        self.web = web
        self.db = db
        self.nginx = nginx


def container_detail(container):
    name = container.name
    image = container.image.tags[0]

    host_ports = []
    container_ports = []
    for container_port, details in container.attrs['NetworkSettings']['Ports'].items():
        container_ports.append(container_port)
        host_ports.append(details[0]['HostPort'])

    server_id = 'none'

    for label, value in container.labels.items():
        if label == 'server_id':
            server_id = value

    return server_id, name, image, host_ports, container_ports


def list_servers():
    client = docker.from_env()
    containers = client.containers.list()
    containers = list(map(container_detail, containers))

    server_containers = {}

    for server_id, name, image, host_ports, container_ports in containers:
        if server_id not in server_containers:
            server_containers[server_id] = []

        container = ServerContainer(name, image, host_ports, container_ports)

        server_containers[server_id].append(container)

    servers = []

    for server_id, containers in server_containers.items():
        web = None
        db = None
        nginx = None

        for container in containers:
            split = container.name.split('_')
            type = split[1] if len(split) > 1 else None
            if type == 'web':
                web = container
            elif type == 'db':
                db = container
            elif type == 'nginx':
                nginx = container

        server = Server(server_id, web, db, nginx)
        servers.append(server)

    return servers


def generate_proxy_config():
    manage_domain = settings.MANAGE_DOMAIN
    port = settings.MANAGE_PORT
    host = 'localhost'

    config = \
        """
        server {{
            listen {port};
            server_name {manage_domain}.*;
            location / {{
                proxy_pass http://{host}:8001;
                proxy_set_header Host $host;
            }}
        }}
        """.format(port=port, manage_domain=manage_domain, host=host)

    servers = list_servers()

    for server in servers:
        if server.nginx is not None:
            if len(server.nginx.host_ports) > 0:
                default = 'default_server' if server.id == 'master' else ''
                config += \
                    '''
                    server {{
                        listen {port} {default};
                        server_name {sid}.*;
                        location / {{
                            proxy_pass http://{host}:{server_port};
                            proxy_set_header Host $host;
                        }}
                    }}
                    '''.format(port=port, default=default, host=host, sid=server.id, server_port=server.nginx.host_ports[0])

    with open('nginx_management/server-management.conf', 'w') as config_file:
        config_file.write(config)


def start_proxy():
    try:
        generate_proxy_config()

        client = docker.from_env()

        cwd = os.getcwd()

        nginx_dir = '{}/nginx_management'.format(cwd)

        containers = client.containers.list(filters={'label': 'proxy'})

        if len(containers) == 0:
            container = client.containers.run(
                'nginx',
                name='proxy',
                detach=True,
                ports={'80': 80},
                network='host',
                volumes={
                    nginx_dir: {'bind': '/etc/nginx/conf.d', 'mode': 'ro'},
                },
                labels=['proxy']
            )

            containers.append(container)

        if len(containers) > 0:
            containers[0].exec_run('/usr/sbin/nginx -s reload', detach=True)

    except Exception as e:
        print("Failed to start proxy: {}".format(e))


def start_server(server_id):
    try:
        registry_url = 'registry.gitlab.com/a-team-rowan-university/web-development/resource-website'
        tag = 'web_{}'.format(server_id)

        client = docker.from_env()

        remove_server(server_id)

        cwd = os.getcwd()

        web_name = "{}_web".format(server_id)
        db_name = "{}_db".format(server_id)
        nginx_name = "{}_nginx".format(server_id)

        network_name = "{}_network".format(server_id)
        static_volume_name = '{}_static'.format(server_id)

        media_dir = '{}/servers/{}/media'.format(cwd, server_id)
        data_dir = '{}/servers/{}/data'.format(cwd, server_id)
        nginx_dir = '{}/nginx_servers'.format(cwd)

        env = {
            'DEBUG': '0',
            'ALLOWED_HOSTS': '*',
            'POSTGRES': '1',
            'POSTGRES_HOST': 'db',
            'POSTGRES_PORT': '5432',
            'POSTGRES_USER': 'postgres',
            'POSTGRES_PASSWORD': settings.MANAGED_POSTGRES_PASSWORD,
            'POSTGRES_DB': 'postgres',
            'IMPORT_SAMPLE_DATA': '1',
        }

        print(env)

        network = client.networks.create(
            name=network_name,
            driver='bridge',
            labels={'server_id': server_id},
        )

        client.volumes.create(
            name=static_volume_name,
            driver_opts={
                'type': 'tmpfs',
                'device': 'tmpfs'
            },
            labels={'server_id': server_id},
        )

        image = client.images.pull(registry_url, tag)

        web_container = client.containers.run(
            image.id,
            name=web_name,
            detach=True,
            ports={'8080': None},
            volumes={
                static_volume_name: {'bind': '/www/static', 'mode': 'rw'},
                media_dir: {'bind': '/www/media', 'mode': 'rw'},
            },
            environment=env,
            labels={'server_id': server_id},
        )

        network.connect(web_container, aliases=['web'])

        db_container = client.containers.run(
            'postgres:10.3-alpine',
            name=db_name,
            detach=True,
            ports={'5432': None},
            volumes={
                data_dir: {'bind': '/var/lib/postgresql/data', 'mode': 'rw'}
            },
            environment=env,
            labels={'server_id': server_id},
        )

        network.connect(db_container, aliases=['db'])

        nginx_container = client.containers.run(
            'nginx',
            name=nginx_name,
            detach=True,
            ports={'80': None},
            volumes={
                nginx_dir: {'bind': '/etc/nginx/conf.d', 'mode': 'ro'},
                static_volume_name: {'bind': '/static', 'mode': 'ro'},
                media_dir: {'bind': '/media', 'mode': 'ro'},
            },
            environment=env,
            labels={'server_id': server_id},
        )

        network.connect(nginx_container, aliases=['nginx'])

        start_proxy()

    except Exception as e:
        print("Failed to start {}: {}".format(server_id, e))


def remove_server(server_id):
    try:
        client = docker.from_env()

        containers = client.containers.list(all=True, filters={'label': 'server_id={}'.format(server_id)})
        for container in containers:
            container.stop()
            container.remove()

        volumes = client.volumes.list(filters={'label': 'server_id={}'.format(server_id)})
        for volume in volumes:
            volume.remove()

        networks = client.networks.list(filters={'label': 'server_id={}'.format(server_id)})
        for network in networks:
            network.remove()

        start_proxy()

    except Exception as e:
        print("Failed removing {}: {}", server_id, e)
